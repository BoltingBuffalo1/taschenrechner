﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rechner.Forms
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "2";
        }

        private void buttonGleich_Click(object sender, EventArgs e)
        {
            float zahl;     // Definition
            string anzeige = textBoxEingabe.Text; // anzeige den wert textboxeingabe gegeben
            if (anzeige.Contains("+") == true)
            {
                string[] zahlenString = anzeige.Split('+'); // das teilen von den zwei zahlen und dem plus
                float zahl1 = float.Parse(zahlenString[0]);
                float zahl2 = float.Parse(zahlenString[1]);
                zahl = (zahl1 + zahl2);
                MessageBox.Show(zahl.ToString());
            }
            else if (anzeige.Contains("-") == true) 
            {
                string[] zahlenString = anzeige.Split('-'); 
                float zahl1 = float.Parse(zahlenString[0]);
                float zahl2 = float.Parse(zahlenString[1]);
                zahl = (zahl1 - zahl2);
                MessageBox.Show(zahl.ToString());
            }
            else if (anzeige.Contains("*") == true)
            {
                string[] zahlenString = anzeige.Split('*');
                float zahl1 = float.Parse(zahlenString[0]);
                float zahl2 = float.Parse(zahlenString[1]);
                zahl = (zahl1 * zahl2);
                MessageBox.Show(zahl.ToString());
            }
            else if (anzeige.Contains("/") == true)
            {
                string[] zahlenString = anzeige.Split('/');
                float zahl1 = float.Parse(zahlenString[0]);
                float zahl2 = float.Parse(zahlenString[1]);
                zahl = (zahl1 / zahl2);
                MessageBox.Show(zahl.ToString());
            }
            else if (anzeige.Contains(":") == true)
            {
                string[] zahlenString = anzeige.Split(':');
                float zahl1 = float.Parse(zahlenString[0]);
                float zahl2 = float.Parse(zahlenString[1]);
                zahl = (zahl1 / zahl2);
                MessageBox.Show(zahl.ToString());
            }
            else
            {
                MessageBox.Show("Es wurde keine Operationszeichen eingegeben.");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
                string str = textBoxEingabe.Text;
                textBoxEingabe.Text = str + "3";
            }

        private void button4_Click(object sender, EventArgs e)
        {
                string str = textBoxEingabe.Text;
                textBoxEingabe.Text = str + "4";
            }

        private void button5_Click(object sender, EventArgs e)
        {
                string str = textBoxEingabe.Text;
                textBoxEingabe.Text = str + "5";
            }

        private void button6_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "7";

        }
        private void button8_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "8";

        }
        private void button9_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "9";

        }
        private void button0_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "0";

        }
        private void button16_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + ",";

        }

        private void button14_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "-";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "+";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + "*";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            string str = textBoxEingabe.Text;
            textBoxEingabe.Text = str + ":";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBoxEingabe.Clear ();
        }
      
    }
}
    
    

